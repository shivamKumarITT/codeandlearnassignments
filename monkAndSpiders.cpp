#include <iostream>
#include <stdio.h>
#include <bits/stdc++.h>

using namespace std;

int main()
{
    int totalSpiders = 0 , spiderNeeded = 0;

    cin >> totalSpiders >> spiderNeeded ;

    queue<int> spiderQueue;

    int spiderPower [totalSpiders + 1];

    for(int currentSpider = 1 ; currentSpider <= totalSpiders ; currentSpider++){
        cin >> spiderPower[currentSpider];
        spiderQueue.push(currentSpider);
    }

    int maxPower = 0 ;
    int maxPowSpiderIndex, currentSpiderIndex ;
    bool reamainningSpiders[totalSpiders+1] ;

    memset(reamainningSpiders,false,sizeof(reamainningSpiders)) ;

    int firstQueueSpider = 0, lastQueueSpider ;
    int leftSpiders = totalSpiders ;

    for (int spiderSelected = 0 ; spiderSelected < spiderNeeded ; spiderSelected++ )
    {
        firstQueueSpider = 0 ;
        lastQueueSpider = min( spiderNeeded , leftSpiders ) ;
        maxPower = -1 ;

        while (firstQueueSpider < lastQueueSpider)
        {
            currentSpiderIndex = spiderQueue.front() ;
            if (reamainningSpiders[currentSpiderIndex])
            {
                spiderQueue.pop() ;
                continue ;
            }
            if (spiderPower[currentSpiderIndex] > maxPower)
            {
                maxPower = spiderPower[currentSpiderIndex] ;
                maxPowSpiderIndex = currentSpiderIndex ;
            }

            spiderQueue.pop() ;
            spiderQueue.push(currentSpiderIndex) ;
            firstQueueSpider++ ;
            if (spiderPower[currentSpiderIndex]) spiderPower[currentSpiderIndex]-=1 ;
        }
        
        reamainningSpiders[maxPowSpiderIndex] = true ;
        cout << maxPowSpiderIndex << " "  ;
        leftSpiders-- ;
    }
    
    cout<<endl ;

    return 0;
}
