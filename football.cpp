#include<stdio.h>
#include <iostream>

using namespace std;

int main()
{
    int testCasesNum = 0;
    cin >> testCasesNum;
    for( int currTestCase = 0 ; currTestCase < testCasesNum ; currTestCase++ ){

        int numOfPasses = 0, firstPlayerID = 0;

        cin >> numOfPasses >> firstPlayerID;

        int currentPlayerID = firstPlayerID;
        int previousPlayerID = firstPlayerID;

        for(int currPass = 0 ; currPass < numOfPasses ; currPass++ ){

            char passType[1];
            int playerID = 0;

            cin >> passType;

            if(passType[0] == 'P'){

                cin >> playerID;
                previousPlayerID = currentPlayerID;
                currentPlayerID = playerID;

            }else{

                int tempID = currentPlayerID ;
                currentPlayerID = previousPlayerID;
                previousPlayerID = tempID;

            }
        }
        cout << "Player " << currentPlayerID << endl;
    }
    return 0;
}
